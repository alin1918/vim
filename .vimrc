" loads the plugins from bundle folder "
execute pathogen#infect()

" highlighting syntax "
syntax on

filetype plugin indent on

"TAB EQUALS 4 stapces "
:set tabstop=4 shiftwidth=4 expandtab

set number nu           "show lines number"
set cursorline
set autoindent
set smartindent  
set incsearch           "search as characters are entered"
set hlsearch            " highlight matches


"SET ANOTHER COLOR SCHEME"
":colorscheme koehler"
:colorscheme slate

"SHOW VERTICAL LINE AT LINE 80"
execute "set colorcolumn=81,121"
"highlight ColorColumn guibg=#303030 ctermbg=0


"PLUGINS"
" FOR NERD TREE "
map <F2> :NERDTreeToggle<CR>

" FOR T Comment "
map <leader>c <c-_><c-_>

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let g:syntastic_php_checkers = ['php', 'phpcs', 'phpmd']
let g:syntastic_php_phpcs_exec = './bin/phpcs'
let g:syntastic_php_phpcs_args = '--standard=psr2'
let g:syntastic_php_phpmd_exec = './bin/phpmd'
let g:syntastic_php_phpmd_post_args = 'cleancode,codesize,controversial,design,unusedcode'
